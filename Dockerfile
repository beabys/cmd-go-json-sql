FROM golang:1.14-stretch as build
ENV CGO_ENABLED=0
WORKDIR /app
COPY go.mod go.sum ./
RUN go mod download
COPY . .
# Build the app
RUN go build -a -o /app/parser

# Use another container after app is build because golag container is to big on space
FROM alpine:latest
COPY --from=build /app/parser /usr/local/bin/parser
# Run the compiled app
ENTRYPOINT [ "parser"]