
# cmd-go-json-sql

## Definition

small scrip to generate a sql file from json translation

## execute insert
to execute please run 
```
DOCKER_VOLUME='app' \
&& FILE_FOLDER=$(pwd) \
&& docker run --rm -ti -v $FILE_FOLDER:/$DOCKER_VOLUME \
registry.gitlab.com/beabys/cmd-go-json-sql:latest create-insert \
-t inbox_tpl_driver_non_public \
-p /$DOCKER_VOLUME \
-o sql.sql
```

## execute update
to execute please run 
```
DOCKER_VOLUME='app' \
&& FILE_FOLDER=$(pwd) \
&& docker run --rm -ti -v $FILE_FOLDER:/$DOCKER_VOLUME \
registry.gitlab.com/beabys/cmd-go-json-sql:latest create-update \
-t inbox_tpl_driver_non_public \
-p /$DOCKER_VOLUME/ \
-o sql.sql \
-k "title" \
-w "hlang,tpl_id"
```

## Flags

### for insert
|  Flag                      |  Type                   |  Definition                                                         | 
|:---------------------------|:------------------------|:--------------------------------------------------------------------|
|  -p, --source-path         | string                  | source path which contains json files to be converted(required)     |
|  -h, --help                | string                  | help for create-insert                                              |
|  -i, --id-name             | string                  | id used on the sql (default "tpl_id")                               |
|  -o, --output-file         | string                  | file where the sql file will be written (default "./default.sql")   |
|  -t, --table-name          | string                  | table name where data will be inserted/updated (required)           |


### for update
|  Flag                      |  Type                   |  Definition                                                         | 
|:---------------------------|:------------------------|:--------------------------------------------------------------------|
|  -p, --source-path         | string                  | source path which contains json files to be converted(required)     |
|  -h, --help                | string                  | help for create-insert                                              |
|  -i, --id-name             | string                  | id used on the sql (default "tpl_id")                               |
|  -o, --output-file         | string                  | folder where the sql file will be written (default "./default.sql") |
|  -t, --table-name          | string                  | table name where data will be inserted/updated (required)           |
|  -k, --ignored-keys        | strings                 | ignored keys                                                        |
|  -w, --and-where-keys      | strings                 | keys used for and where condition                                   |
|  -n, --or-where-keys       | strings                 | keys used for or where condition                                    |
|  -o, --output-file         | string                  | file where the sql file will be written (default "./default.sql")   |