package main

import (
	"gitlab.com/beabys/cmd-go-json-sql/cmd"
)

func main() {
	cmd.Execute()
}
