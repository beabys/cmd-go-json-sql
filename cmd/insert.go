package cmd

import (
	"fmt"
	"os"
	"path/filepath"
	"sort"
	"strings"

	// "github.com/davecgh/go-spew/spew"

	"github.com/spf13/cobra"
)

// insertCmd represents the convert command
var insertCmd = &cobra.Command{
	Use:   "create-insert",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		//validate file json exist
		err, files := getFiles(&sourcePath)
		if err != nil {
			fmt.Println(err.Error())
			os.Exit(0)
		}
		for _, file := range files {
			exPath := filepath.Dir(file) + "/insert-" + outputFile
			mapped, err := parse(file)
			if err != nil {
				fmt.Println(err.Error())
				os.Exit(0)
			}
			err = insertToSQL(mapped, exPath)
			if err != nil {
				fmt.Println(err.Error())
				os.Exit(0)
			}
			// spew.Dump(mapped)
		}

	},
}

// init function
func init() {
	//adding required flag parmeters
	insertCmd.Flags().StringVarP(&sourcePath, "source-path", "p", "", "json file to be converted (required)")
	insertCmd.Flags().StringVarP(&tableName, "table-name", "t", "", "table name where data will be inserted/updated (required)")
	insertCmd.Flags().StringVarP(&idName, "id-name", "i", "tpl_id", "id used on the sql")
	insertCmd.Flags().StringVarP(&outputFile, "output-file", "o", "default.sql", "folder where the sql file will be written")
	//required flags
	insertCmd.MarkFlagRequired("source-path")
	insertCmd.MarkFlagRequired("table-name")

	// appending command convert into root command
	rootCmd.AddCommand(insertCmd)
}

func insertToSQL(mapped data, outputFile string) error {
	sql := "INSERT INTO %v (%v) VALUES (%v);"
	var toFile []string
	for _, structToWork := range mapped {
		var keys, values []string
		for key := range structToWork {
			keys = append(keys, key)
		}
		sort.Sort(sort.Reverse(sort.StringSlice(keys)))
		for _, key := range keys {
			values = append(values, "\""+structToWork[key]+"\"")
		}
		query := fmt.Sprintf(sql, tableName, strings.Join(keys, ", "), strings.Join(values, ", "))
		toFile = append(toFile, query)
	}
	file, err := os.OpenFile(outputFile, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
	// file, err := os.Create(outputFile)
	if err != nil {
		return err
	}
	defer file.Close()

	file.WriteString(strings.Join(toFile, "\n"))
	return nil
}
