package cmd

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
	"path/filepath"
	"strings"
	// "github.com/davecgh/go-spew/spew"
)

// fileExist validates if file exist
func fileExist(file string) bool {
	info, err := os.Stat(file)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

// parse the json file into generic struct
func parse(file string) (data, error) {
	var structGeneric map[string]structToWork
	jsonFile, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer jsonFile.Close()
	byteValue, _ := ioutil.ReadAll(jsonFile)
	err = json.Unmarshal([]byte(byteValue), &structGeneric)
	if err != nil {
		return nil, err
	}

	var data data
	for id, genericData := range structGeneric {
		genericData[idName] = id
		data = append(data, genericData)
	}
	return data, nil
}

func confirm() bool {
	reader := bufio.NewReader(os.Stdin)
	for {
		s, _ := reader.ReadString('\n')
		s = strings.TrimSuffix(s, "\n")
		s = strings.ToLower(s)
		if len(s) > 1 {
			fmt.Fprintln(os.Stderr, "Please enter Y or N")
			continue
		}
		if strings.Compare(s, "n") == 0 {
			return false
		} else if strings.Compare(s, "y") == 0 {
			break
		} else {
			continue
		}
	}
	return true
}

func visit(files *[]string) filepath.WalkFunc {
	return func(path string, info os.FileInfo, err error) error {
		if err != nil {
			log.Fatal(err)
		}
		*files = append(*files, path)
		return nil
	}
}

func getFiles(path *string) (error, []string) {
	var files []string
	var filesToProccess []string
	var root string = *path
	err := filepath.Walk(root, visit(&files))
	if err != nil {
		return err, nil
	}
	for _, file := range files {
		if filepath.Ext(file) == ".json" {
			filesToProccess = append(filesToProccess, file)
		}
	}
	return nil, filesToProccess
}

func filenameWithoutExtension(fn string) string {
	return strings.TrimSuffix(fn, path.Ext(fn))
}
