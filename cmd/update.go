package cmd

import (
	"fmt"
	"os"
	"path/filepath"
	"sort"
	"strings"

	// "github.com/davecgh/go-spew/spew"

	"github.com/spf13/cobra"
)

// updateCmd represents the convert command
var updateCmd = &cobra.Command{
	Use:   "create-update",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		//validate file json exist
		err, files := getFiles(&sourcePath)
		if err != nil {
			fmt.Println(err.Error())
			os.Exit(0)
		}
		for _, file := range files {

			newFile := filepath.Dir(file) + "/update-" + outputFile
			mapped, err := parse(file)
			if err != nil {
				fmt.Println(err.Error())
				os.Exit(0)
			}
			err = updateToSQL(mapped, newFile)
			if err != nil {
				fmt.Println(err.Error())
				os.Exit(0)
			}
			// spew.Dump(mapped)
		}

	},
}

// init function
func init() {
	//adding required flag parmeters
	updateCmd.Flags().StringVarP(&sourcePath, "source-path", "p", "", "json file to be converted (required)")
	updateCmd.Flags().StringVarP(&tableName, "table-name", "t", "", "table name where data will be inserted/updated (required)")
	updateCmd.Flags().StringVarP(&idName, "id-name", "i", "tpl_id", "id used on the sql")
	updateCmd.Flags().StringSliceVarP(&andWhereKeys, "and-where-keys", "w", andWhereKeys, "keys used for where condition")
	updateCmd.Flags().StringSliceVarP(&orWhereKeys, "or-where-keys", "n", orWhereKeys, "keys used for where condition")
	updateCmd.Flags().StringSliceVarP(&ignoredKeys, "ignored-keys", "k", ignoredKeys, "ignored keys")
	updateCmd.Flags().StringVarP(&outputFile, "output-file", "o", "default.sql", "folder where the sql file will be written")
	//required flags
	updateCmd.MarkFlagRequired("source-path")
	updateCmd.MarkFlagRequired("table-name")

	// appending command convert into root command
	rootCmd.AddCommand(updateCmd)
}

func updateToSQL(mapped data, outputFile string) error {
	sql := "UPDATE %v SET %v WHERE %v;"
	var toFile []string
	for _, structToWork := range mapped {
		var keys, values, andWhere, orWhere []string
		for key := range structToWork {
			keys = append(keys, key)
		}
		sort.Sort(sort.Reverse(sort.StringSlice(keys)))
		for _, key := range keys {
			switch true {
			case existInSlice(andWhereKeys, key):
				andWhere = append(andWhere, key+" = \""+structToWork[key]+"\"")
				break
			case existInSlice(orWhereKeys, key):
				orWhere = append(orWhere, key+" = \""+structToWork[key]+"\"")
				break
			case existInSlice(ignoredKeys, key):
				break
			default:
				values = append(values, key+" = \""+structToWork[key]+"\"")
				break
			}
		}
		query := fmt.Sprintf(sql, tableName, strings.Join(values, ", "), strings.Join(andWhere, " AND "))
		if len(orWhere) > 0 {
			query = query + "OR " + strings.Join(orWhere, " OR ")
		}
		toFile = append(toFile, query)
		// query := fmt.Sprintf(sql, tableName, strings.Join(values, ", "), "1=1")
		// toFile = append(toFile, query)
	}
	file, err := os.OpenFile(outputFile, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
	// file, err := os.Create(outputFile)
	if err != nil {
		return err
	}
	defer file.Close()

	file.WriteString(strings.Join(toFile, "\n"))
	return nil
}

func existInSlice(slice []string, k string) bool {
	for _, s := range slice {
		if k == s {
			return true
		}
	}
	return false
}
