package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

// define th types
type structToWork map[string]string
type data []structToWork

var sourcePath, tableName, outputFile, idName string
var andWhereKeys, orWhereKeys, ignoredKeys []string
var fileContent []byte
var verbose, ask bool

var rootCmd = &cobra.Command{
	Use:   "parser",
	Short: "parser to convert json translation file to sql file",
	Long:  `small and simple parser to convert json translations file to sql file"`,
}

// Execute will execute the main action
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
